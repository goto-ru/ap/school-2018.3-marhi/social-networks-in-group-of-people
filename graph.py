import networkx as nx
import json
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
# import plotly.plotly as pl
import plotly
from plotly.graph_objs import Scattergl, Scatter, Figure, Layout
from tqdm import tqdm

print("LOAD")
friends1 = json.load(open("friends.json"))
friends2 = json.load(open("friends2.json"))
data = json.load(open("birth_day_with_fields.json"))

info_by_user = {}
info_by_user2 = {}

for l in data:
    for obj in l:
        info_by_user[str(obj['id'])] = obj['sex']
        info_by_user2[str(obj['id'])] = "<a src='http://vk.com/id{}'>{} {}</a>".format(obj['id'], obj['first_name'], obj['last_name'])
print(len(info_by_user))
len(friends1)
len(friends2)

G = nx.Graph(directed=False)
print("ADD")

from collections import Counter
sizes = Counter()

for key in tqdm(friends1):
    for friend in friends1[key]:
        G.add_edge(key, friend)
        sizes[key] += 1

for key in tqdm(friends2):
    for friend in friends2[key]:
        G.add_edge(key, friend)
        sizes[key] += 1

with_text = []

for i in sizes.most_common(100):
    with_text.append(i[0])

for i in sizes:
    sizes[i] /= 10

print("POS")
pos = nx.random_layout(G)

x=[]
y=[]

x_text, y_text, labels = [], [], []

sizes_real = []
texts = []
colors = []
for i in tqdm(pos):
    if i in with_text:
        x_text.append(pos[i][0])
        y_text.append(pos[i][1])
        labels.append(info_by_user2[str(i)])
    x.append(pos[i][0])
    y.append(pos[i][1])
    sizes_real.append(sizes[i])
    texts.append("<a href='https://vk.com/id{0}'>{0}</a>".format(i))

    if str(i) in info_by_user.keys():
        colors.append(info_by_user[str(i)])
    else:
        colors.append(-1)
print(Counter(colors))
def _arrow(x, y, x2, y2):
    return dict(
        ax=x2,
        ay=y2,
        axref='x',
        ayref='y',
        x=x,
        y=y,
        opacity=0.1,
        xref='x',
        yref='y',
        showarrow=True,
        arrowhead=0,
        arrowsize=2,
        arrowwidth=1,
        arrowcolor='#636363'
    )
print("arrows")
arrows = []
for edge in G.edges():
    x0, y0 = (pos[(list(edge)[0])])
    x1, y1 = (pos[(list(edge)[1])])

    arrows.append(_arrow(x1, y1, x0, y0))

# start=[]
# end=[]
#
# for edge in G.edges():
#     start.append(pos[(list(edge)[0])])
#     end.append(pos[(list(edge)[1])])
#
# print(start)
# print(end)
print("data")
data = [Scattergl(x=x, y=y, mode='markers+text', opacity=0.5, marker={'size': sizes_real,
            "color": colors, #set color equal to a variable
            "colorscale":'Viridis'}), Scatter(
        x=x_text,
        y=y_text,
        mode='markers+text',
        name='Markers and Text',
        text=labels,
        textposition='top',
        hoverinfo='none',
        showlegend = False,
    )]

print("layout")
# layout = Layout(showlegend=False, annotations=arrows)
print("fig")
fig = Figure(data=data)
print("draw!")
plot(fig)